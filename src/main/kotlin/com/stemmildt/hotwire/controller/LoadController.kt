package com.stemmildt.hotwire.controller

import com.stemmildt.hotwire.service.LoadService
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.thymeleaf.spring6.context.webflux.ReactiveDataDriverContextVariable

@Controller
@RequestMapping("/load")
class LoadController(private val loadService: LoadService) {

  @GetMapping(produces = [MediaType.TEXT_EVENT_STREAM_VALUE])
  suspend fun load(model: Model) =
    model.let {
      it.addAttribute("loadStream", ReactiveDataDriverContextVariable(loadService.stream(), 1))
      "load"
    }
}
