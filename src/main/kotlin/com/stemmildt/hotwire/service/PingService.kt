package com.stemmildt.hotwire.service

import com.stemmildt.hotwire.domain.PingResult
import kotlinx.coroutines.delay
import org.springframework.stereotype.Service
import java.io.IOException
import java.net.InetSocketAddress
import java.nio.channels.AsynchronousSocketChannel
import java.util.concurrent.TimeUnit
import kotlin.math.absoluteValue
import kotlin.random.Random
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

@Service
class PingService {

  @ExperimentalTime
  suspend fun ping(hostname: String, port: Int) =
    PingResult(
      runCatching {
        measureTime {
          AsynchronousSocketChannel.open().use {
            it.connect(InetSocketAddress(hostname, port)).get(10, TimeUnit.SECONDS)
            fakeLatency()
          }
        }.inWholeMilliseconds
      }.getOrDefault(-1)
    )

  /**
   * The following is purely for some randomness to simulate ping times.
   * The randomness doesn't occur for unit tests (as it uses a mocked AsynchronousSocketChannel).
   * None of this sort of code would appear in a real app.
   */
  private suspend fun fakeLatency() {
    val sleep = Random.nextLong().absoluteValue % 10
    if (sleep % 5 == 0L) {
      throw IOException() // force some timeouts now and then
    }
    delay(sleep)
  }
}
