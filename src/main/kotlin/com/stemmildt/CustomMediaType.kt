package com.stemmildt

import org.springframework.http.MediaType

object CustomMediaType {

  val TURBO_STREAM = MediaType("text", "vnd.turbo-stream.html")
  const val TURBO_STREAM_VALUE = "text/vnd.turbo-stream.html"
}
