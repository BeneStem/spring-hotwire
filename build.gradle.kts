import com.github.benmanes.gradle.versions.reporter.PlainTextReporter
import com.github.benmanes.gradle.versions.reporter.result.Result
import com.github.benmanes.gradle.versions.updates.DependencyUpdatesTask
import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.extensions.DetektExtension
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.owasp.dependencycheck.gradle.extension.AnalyzerExtension

plugins {
  kotlin("jvm") version "1.8.22"
  kotlin("plugin.spring") version "1.8.22"
  id("org.springframework.boot") version "3.1.1"
  id("io.spring.dependency-management") version "1.1.0"
  id("org.jetbrains.kotlinx.kover") version "0.7.2"
  id("org.jmailen.kotlinter") version "3.15.0"
  id("io.gitlab.arturbosch.detekt") version "1.23.0"
  id("org.owasp.dependencycheck") version "8.3.1"
  id("com.github.ben-manes.versions") version "0.47.0"
}

group = "com.stemmildt"
version = "0.0.1"

repositories {
  mavenCentral()
}

dependencies {
  implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
  implementation("io.projectreactor.kotlin:reactor-kotlin-extensions")
  implementation("org.jetbrains.kotlinx:kotlinx-coroutines-reactor:1.7.1")

  implementation("org.springframework.boot:spring-boot-starter-webflux")
  implementation("org.springframework.boot:spring-boot-starter-thymeleaf")
  implementation("com.fasterxml.jackson.module:jackson-module-kotlin:2.15.2")

  developmentOnly("org.springframework.boot:spring-boot-devtools")
  developmentOnly("io.projectreactor:reactor-tools")

  detektPlugins("io.gitlab.arturbosch.detekt:detekt-formatting:1.23.0")

  annotationProcessor("org.springframework:spring-context-indexer:6.0.10")
}

configure<DetektExtension> {
  config.from(files("$rootDir/detekt-config.yml"))
}

dependencyCheck {
  analyzers(closureOf<AnalyzerExtension> {
    assemblyEnabled = false
    failBuildOnCVSS = 5F
    failOnError = true
    suppressionFile = "./gradle/config/suppressions.xml"
  })
}

java {
  sourceCompatibility = JavaVersion.VERSION_17
  targetCompatibility = JavaVersion.VERSION_17
}

tasks {
  withType<Detekt> {
    jvmTarget = JavaVersion.VERSION_17.toString()
  }

  withType<KotlinCompile> {
    kotlinOptions {
      jvmTarget = JavaVersion.VERSION_17.toString()
      apiVersion = "2.0"
      languageVersion = "2.0"
      javaParameters = true
      freeCompilerArgs = listOf("-Xjsr305=strict")
    }
  }

  withType<Test> {
    useJUnitPlatform()
  }

  withType<DependencyUpdatesTask> {
    fun isStable(version: String): Boolean {
      val stableKeyword = listOf("RELEASE", "FINAL", "GA").any { version.uppercase().contains(it) }
      val regex = "^[0-9,.v-]+(-r)?$".toRegex()
      return stableKeyword || regex.matches(version)
    }
    rejectVersionIf {
      isStable(currentVersion) && !isStable(candidate.version)
    }
    outputFormatter = closureOf<Result> {
      PlainTextReporter(project, System.getProperty("revision")
        ?: "milestone", gradleReleaseChannel).write(System.out, this)
      if (this.outdated.dependencies.isNotEmpty()) {
        throw GradleException("Abort, there are dependencies to update.")
      }
    }
  }
}
