package com.stemmildt.hotwire.config

import com.stemmildt.CustomMediaType
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.thymeleaf.spring6.SpringWebFluxTemplateEngine
import org.thymeleaf.spring6.view.reactive.ThymeleafReactiveViewResolver
import java.nio.charset.StandardCharsets

@Configuration
class ThymeleafConfig {

  @Bean
  fun viewResolver(templateEngine: SpringWebFluxTemplateEngine?) =
    ThymeleafReactiveViewResolver().also {
      it.supportedMediaTypes = listOf(CustomMediaType.TURBO_STREAM)
      it.defaultCharset = StandardCharsets.UTF_8
      it.order = 0
      it.viewNames = arrayOf("*")
      it.templateEngine = templateEngine
    }
}
