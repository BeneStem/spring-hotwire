package com.stemmildt.hotwire.domain

class PingResult(val value: Long) {

  override fun toString() =
    if (value < 0) {
      "timeout"
    } else {
      "$value ms"
    }
}
