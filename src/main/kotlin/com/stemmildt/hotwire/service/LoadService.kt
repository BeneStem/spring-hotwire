package com.stemmildt.hotwire.service

import com.stemmildt.hotwire.domain.SystemLoadAverage
import kotlinx.coroutines.reactive.asFlow
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import java.lang.management.ManagementFactory
import java.lang.management.OperatingSystemMXBean
import java.time.Duration

@Service
class LoadService {

  private val osMxBean: OperatingSystemMXBean = ManagementFactory.getOperatingSystemMXBean()

  suspend fun stream() =
    Flux.interval(Duration.ofSeconds(3))
      .map { SystemLoadAverage(osMxBean.systemLoadAverage) }
      .asFlow()
}
