package com.stemmildt.hotwire.controller

import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.RequestMapping

@Controller
class HomeController {

  @RequestMapping("/")
  fun home() = "index"
}
