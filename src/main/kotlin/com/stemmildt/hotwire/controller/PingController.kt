package com.stemmildt.hotwire.controller

import com.stemmildt.CustomMediaType
import com.stemmildt.hotwire.service.PingService
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.MediaType
import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.RequestMapping
import kotlin.time.ExperimentalTime

@ExperimentalTime
@Controller
@RequestMapping("/pinger")
class PingController(private val pingService: PingService) {

  @Value("\${ping.hostname:127.0.0.1}")
  private val hostname: String = "127.0.0.1"

  @Value("\${ping.port:99}")
  private val port: Int = 99

  @RequestMapping(produces = [MediaType.TEXT_HTML_VALUE, CustomMediaType.TURBO_STREAM_VALUE])
  suspend fun pinger(model: Model) =
    model.let {
      it.addAttribute("pingTime", pingService.ping(hostname, port))
      "ping"
    }
}
